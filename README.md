# Gestión de Notas

Este proyecto es una aplicación de gestión de notas construida con Next.js y TypeScript. Permite a los usuarios registrarse, autenticarse, y gestionar sus notas personales.

## Características

- **Autenticación de usuarios**: Los usuarios pueden registrarse, iniciar sesión y cerrar sesión.
- **CRUD de notas**: Los usuarios pueden crear, leer, actualizar y eliminar notas.
- **Diseño responsivo**: Interfaz optimizada para dispositivos móviles.

## Tecnologías Utilizadas

- Next.js
- TypeScript
- Tailwind CSS
- Supabase

## Configuración del Proyecto

### Requisitos Previos

Asegúrate de tener instalado Node.js y npm en tu sistema. Puedes descargarlos desde [Node.js official website](https://nodejs.org/).

### Instalación

Clona este repositorio y navega al directorio del proyecto:

```bash
git clone git@gitlab.com:anddy665/backend-from-frontend.git
cd backend-from-frontEnd

