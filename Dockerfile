# Elegir una imagen base
FROM node:18-alpine

# Establecer directorio de trabajo
WORKDIR /app

# Copiar los archivos de dependencias y instalarlas
COPY package.json ./
RUN npm install --frozen-lockfile

# Copiar el resto del código fuente del proyecto
COPY . .

# install supabase and others dependencies of npm
RUN npm install @supabase/supabase-js @supabase/ssr
RUN npm install 

# Construir la aplicación
RUN npm run build

# Exponer el puerto que utiliza Next.js
EXPOSE 3000

# Definir el comando para ejecutar la aplicación
CMD ["npm", "run", "dev"]