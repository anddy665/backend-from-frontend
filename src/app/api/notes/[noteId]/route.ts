import { createClient } from "@/utils/supabase/server"
import { NextRequest, NextResponse } from "next/server"

export async function GET(
  request: NextRequest,
  { params }: { params: { noteId: string } }
) {
  const supabase = createClient();
  const noteId = params.noteId;

  const { data: notes, error } = await supabase
    .from("notes")
    .select("*")
    .eq("id", noteId)
    .single();

  if (error) {
    return NextResponse.json({ error }, { status: 404 });
  }

  return NextResponse.json({ notes });
}

export async function PATCH(
  request: NextRequest,
  { params }: { params: { noteId: string } }
) {
  const supabase = createClient();
  const noteId = params.noteId;

  if (request.headers.get("content-type")?.includes("application/json")) {
    const { title, note } = await request.json();

    const { data, error } = await supabase
      .from("notes")
      .update({ title, note })
      .eq( "id", noteId ) 
      .select();

    if (error) {
      return new NextResponse(JSON.stringify({ error }), { status: 404 });
    }

    return new NextResponse(JSON.stringify(data), { status: 200 });
  }

  return new NextResponse(JSON.stringify({ error: "Invalid request" }), { status: 400 });
}


export async function DELETE(
  request: NextRequest,
  { params }: { params: { noteId: string } }
) {
  const supabase = createClient()
  const noteId = params.noteId

  const { error, count, status } = await supabase
    .from("notes")
    .delete()
    .eq("id", noteId)

  if (error) {
    return NextResponse.json({ error }, { status: 404 })
  }

  return NextResponse.json({ success: true })
}
