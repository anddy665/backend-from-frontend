import { createClient } from "@/utils/supabase/server"
import { NextRequest, NextResponse } from "next/server"

export async function GET() {
  const supabase = createClient()

  const { data: { user }, error: userError } = await supabase.auth.getUser();

  if (!user || userError) {
    return new NextResponse(JSON.stringify({ error: "You need to log in to access this information." }), {
      status: 401, 
      headers: {
        'Content-Type': 'application/json'
      }
    });
  }

  const { data: notes, error } = await supabase
  .from("notes")
  .select("*")
  .eq("user_id", user?.id)

  if (error) {
    return NextResponse.json({ error }, { status: 500 })
  }

  return NextResponse.json({ notes })
}

export async function POST(request: NextRequest) {
  const supabase = createClient()

  const { title, note } = await request.json()

  const {
    data: { user },
  } = await supabase.auth.getUser()

  const { data, error } = await supabase
    .from("notes")
    .insert([{ title, note, user_id: user?.id }])
    .single()

  if (error) {
    return NextResponse.json({ error: error.message }, { status: 400 });
  }

  return NextResponse.json(data, { status: 201 })
}
