"use client"
import React, { useEffect, useState } from "react";
import { useRouter, useParams } from "next/navigation"


interface Note {
  title: string;
  note: string;
}

const EditNoteForm: React.FC = () => {
  const [note, setNote] = useState<Note | null>(null);
  const [loading, setLoading] = useState(true);
  const router = useRouter()
  const params = useParams(); 
  const noteId = params.noteId; 
  
  useEffect(() => {
    const fetchNote = async () => {
      if (noteId) { // Ensure noteId is present
        try {
          const response = await fetch(`/api/notes/${noteId}`);
          
          if (!response.ok) {
            throw new Error("Error fetching note");
          }
          const data = await response.json();
          setNote(data.notes); // Assuming the API returns the note directly
        } catch (error) {
          console.error(error);
        } finally {
          setLoading(false);
        }
      }
    };

    fetchNote();
  }, [noteId]); // Adding noteId as a dependency to useEffect

  if (loading) {
    return <p>Loading...</p>;
  }

  if (!note) {
    return <p>No note found</p>;
  }

  const handleSubmit = async (event: React.FormEvent<HTMLFormElement>) => {
    event.preventDefault();
    // Handling the update submission
    const updatedNote = {
      title: note.title,
      note: note.note,
    };

    await fetch(`/api/notes/${noteId}`, {
      method: "PATCH",
      headers: {
        "Content-Type": "application/json",
      },
      body: JSON.stringify(updatedNote),
    });

    router.push("/"); // Redirect after form submission
  };

  return (
    <form onSubmit={handleSubmit} className="space-y-4">
      <div>
        <label htmlFor="title" className="block text-sm font-medium text-white">Title</label>
        <input
          type="text"
          id="title"
          name="title"
          value={note.title}
          onChange={(e) => setNote({ ...note, title: e.target.value })}
          required
          className="mt-1 block w-full rounded-md bg-gray-800 border-gray-600 text-white"
        />
      </div>
      <div>
        <label htmlFor="content" className="block text-sm font-medium text-white">Content</label>
        <textarea
          id="content"
          name="content"
          value={note.note}
          onChange={(e) => setNote({ ...note, note: e.target.value })}
          required
          rows={4}
          className="mt-1 block w-full rounded-md bg-gray-800 border-gray-600 text-white"
        />
      </div>
      <div>
        <button type="submit" className="inline-flex justify-center py-2 px-4 border border-transparent shadow-sm text-sm font-medium rounded-md text-white bg-blue-600 hover:bg-blue-700">
          Update Note
        </button>
      </div>
    </form>
  );
};

export default EditNoteForm;
