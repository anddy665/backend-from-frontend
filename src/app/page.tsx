import { createClient } from "@/utils/supabase/server"
import NotesComponent from "@/components/NoteComponents"

export default async function Index() {
  const canInitSupabaseClient = () => {
    try {
      createClient()
      return true
    } catch (e) {
      return false
    }
  }

  const isSupabaseConnected = canInitSupabaseClient()

  return (
    <div>
      <NotesComponent />
    </div>
  )
}
