import React from 'react';
import { PencilIcon, TrashIcon } from "@heroicons/react/24/solid";
import Link from "next/link";

interface NoteCardProps {
  id: string;
  title: string;
  note: string;
  date_created: Date;
  date_updated: Date;
}

const NoteCard: React.FC<NoteCardProps> = ({
  id,
  title,
  note,
  date_created,
  date_updated,
}) => {
  const formatDate = (date: Date) => new Date(date).toLocaleDateString("en-US", {
    year: "numeric",
    month: "long",
    day: "numeric",
  });

  const handleDelete = async () => {
    if (window.confirm('Are you sure you want to delete this note?')) {
      const response = await fetch(`/api/notes/${id}`, {
        method: 'DELETE',
      });
      if (response.ok) {
        alert('Note deleted successfully.');
        // refresh the page
        window.location.reload();
      } else {
        alert('Failed to delete the note.');
      }
    }
  };

  return (
    <div className={`rounded-lg shadow-md p-4 flex flex-col justify-between bg-gray-800`}>
      <div>
        <h3 className={`text-lg font-semibold mb-2 text-white`}>{title}</h3>
        <p className={`mb-4 text-gray-300`}>{note}</p>
      </div>
      <div className="flex flex-col">
        <div className="flex flex-col text-sm text-gray-400">
          <span>Created: {formatDate(date_created)}</span>
          {date_updated && (
            <span>Updated: {formatDate(date_updated)}</span>
          )}
        </div>
        <div className="flex ml-auto mt-3 justify-end">
          <Link href={`/notes/edit/${id}`} className={`rounded-full p-2 bg-blue-600 bg-opacity-50 hover:bg-opacity-70 mr-2`} aria-label="Edit note">
            <PencilIcon className={`h-4 w-4 text-white text-opacity-90`} />
          </Link>
          <button
            onClick={handleDelete}
            className={`rounded-full p-2 bg-red-500 bg-opacity-50 hover:bg-opacity-70`}
            aria-label="Delete note"
          >
            <TrashIcon className={`h-4 w-4 text-white text-opacity-90`} />
          </button>
        </div>
      </div>
    </div>
  );
}

export default NoteCard;
