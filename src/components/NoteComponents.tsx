"use client"
import React, { useEffect, useState } from "react";
import NoteCard from "./NoteCard";
import { PencilSquareIcon } from "@heroicons/react/24/solid";
import Link from "next/link";

export interface Note {
  id: string;
  title: string;
  note: string;
  created_at: Date;
  update_at: Date;
}

// Grid container for better layout management
const GridContainer = ({ children }: { children: React.ReactNode }) => (
  <div className="grid grid-cols-1 sm:grid-cols-2 md:grid-cols-3 lg:grid-cols-4 gap-4">
    {children}
  </div>
);

const NotesComponent: React.FC = () => {
  const [notes, setNotes] = useState<Note[]>([]);
  const [isLoading, setIsLoading] = useState(true);
  const [error, setError] = useState('');


  useEffect(() => {
    const fetchNotes = async () => {
      const response = await fetch("/api/notes");      
      if (response.status === 401) {
        const errorData = await response.json(); 
        setIsLoading(false);
        setError(errorData.error || "You need to log in to access this information.");
      }
      if (response.ok) {
        const data = await response.json();
        setNotes(data.notes);
        setIsLoading(false);
      }
    };
    fetchNotes();
  }, []);

  if (isLoading) return <div>Loading...</div>;
  if (error) return <div>Error: {error}</div>;


  return (
    <div className="text-white h-screen p-8">
      <div className="mb-8">
      </div>
      <GridContainer>
        {notes.map((note) => (
          <NoteCard
            key={note.id}
            id={note.id}
            title={note.title}
            note={note.note}
            date_created={note.created_at}
            date_updated={note.update_at}
          />
        ))}
      </GridContainer>
      <Link
        href="/notes/create"
        className="fixed right-8 bottom-8 bg-blue-500 bg-opacity-70 hover:bg-opacity-80 p-4 rounded-full shadow-lg text-white text-xl transition duration-300 ease-in-out transform hover:scale-110 flex items-center justify-center"
        aria-label="Create note"
        passHref
      >
        <PencilSquareIcon className="h-6 w-6" />
      </Link>
    </div>
  );
};

export default NotesComponent;
